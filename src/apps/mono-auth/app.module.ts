import { Module, DynamicModule, Provider } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { CoreModule } from '@mono-auth/core-nestjs';
import {
  AuthModule,
  configs,
  controllers,
  services
} from '@mono-auth/auth-nestjs';
import { MonoAuthModule } from './mono-auth.module';

@Module({
  imports: [TypeOrmModule.forRoot(), CoreModule, AuthModule, MonoAuthModule]
})
export class AppModule {
  static forRoot(options: { providers: Provider[] }): DynamicModule {
    return {
      module: AppModule,
      imports: [
        TypeOrmModule.forRoot(),
        CoreModule.forRoot(options),
        AuthModule.forRoot(options),
        MonoAuthModule.forRoot(options)
      ],
      controllers: [...controllers],
      providers: [...configs, ...services]
    };
  }
}
