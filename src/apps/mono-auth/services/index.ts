import { CodeVerifiesService } from './code-verifies.service';
import { MobilesService } from './mobiles.service';
import { TestingsService } from './testings.service';

export const services = [CodeVerifiesService, MobilesService, TestingsService];
