import { Inject, Injectable, MethodNotAllowedException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CORE_CONFIG_TOKEN } from '@mono-auth/core-nestjs';
import { CodeVerify } from '../entities/code-verify.entity';
import { ICoreConfig } from '@mono-auth/core-nestjs';

@Injectable()
export class CodeVerifiesService {
  constructor(
    @Inject(CORE_CONFIG_TOKEN) private readonly coreConfig: ICoreConfig,
    @InjectRepository(CodeVerify)
    private readonly repository: Repository<CodeVerify>
  ) {}
  async create(options: { item: CodeVerify }) {
    try {
      options.item = await this.repository.save(options.item);
      return { codeVerify: options.item };
    } catch (error) {
      throw error;
    }
  }
  async update(options: { id: number; item: CodeVerify }) {
    if (this.coreConfig.demo) {
      throw new MethodNotAllowedException('Not allowed in DEMO mode');
    }
    options.item.id = options.id;
    try {
      options.item = await this.repository.save(options.item);
      return { codeVerify: options.item };
    } catch (error) {
      throw error;
    }
  }
  async delete(options: { id: number }) {
    if (this.coreConfig.demo) {
      throw new MethodNotAllowedException('Not allowed in DEMO mode');
    }
    try {
      let item = await this.repository.findOneOrFail(options.id, {
        relations: []
      });
      item = await this.repository.save(item);
      await this.repository.delete(options.id);
      return { codeVerify: null };
    } catch (error) {
      throw error;
    }
  }
  async findById(options: { id: number }) {
    try {
      const item = await this.repository.findOneOrFail(options.id, {
        relations: []
      });
      return { codeVerify: item };
    } catch (error) {
      throw error;
    }
  }
  async findAll(options: {
    curPage: number;
    perPage: number;
    q?: string;
    sort?: string;
  }) {
    try {
      let objects: [CodeVerify[], number];
      let qb = this.repository.createQueryBuilder('codeVerify');
      if (options.q) {
        qb = qb.where('codeVerify.name like :q or codeVerify.id = :id', {
          q: `%${options.q}%`,
          id: +options.q
        });
      }
      options.sort =
        options.sort &&
        new CodeVerify().hasOwnProperty(options.sort.replace('-', ''))
          ? options.sort
          : '-id';
      const field = options.sort.replace('-', '');
      if (options.sort)
        if (options.sort[0] === '-') {
          qb = qb.orderBy('codeVerify.' + field, 'DESC');
        } else {
          qb = qb.orderBy('codeVerify.' + field, 'ASC');
        }
      qb = qb
        .skip((options.curPage - 1) * options.perPage)
        .take(options.perPage);
      objects = await qb.getManyAndCount();
      return {
        codeVerifies: objects[0],
        meta: {
          perPage: options.perPage,
          totalPages:
            options.perPage > objects[1]
              ? 1
              : Math.ceil(objects[1] / options.perPage),
          totalResults: objects[1],
          curPage: options.curPage
        }
      };
    } catch (error) {
      throw error;
    }
  }
}
