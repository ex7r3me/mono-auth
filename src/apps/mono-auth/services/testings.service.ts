import { Inject, Injectable, MethodNotAllowedException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CORE_CONFIG_TOKEN } from '@mono-auth/core-nestjs';
import { Testing } from '../entities/testing.entity';
import { ICoreConfig } from '@mono-auth/core-nestjs';

@Injectable()
export class TestingsService {
  constructor(
    @Inject(CORE_CONFIG_TOKEN) private readonly coreConfig: ICoreConfig,
    @InjectRepository(Testing)
    private readonly repository: Repository<Testing>
  ) {}
  async create(options: { item: Testing }) {
    try {
      options.item = await this.repository.save(options.item);
      return { testing: options.item };
    } catch (error) {
      throw error;
    }
  }
  async update(options: { id: number; item: Testing }) {
    if (this.coreConfig.demo) {
      throw new MethodNotAllowedException('Not allowed in DEMO mode');
    }
    options.item.id = options.id;
    try {
      options.item = await this.repository.save(options.item);
      return { testing: options.item };
    } catch (error) {
      throw error;
    }
  }
  async delete(options: { id: number }) {
    if (this.coreConfig.demo) {
      throw new MethodNotAllowedException('Not allowed in DEMO mode');
    }
    try {
      let item = await this.repository.findOneOrFail(options.id, {
        relations: []
      });
      item = await this.repository.save(item);
      await this.repository.delete(options.id);
      return { testing: null };
    } catch (error) {
      throw error;
    }
  }
  async findById(options: { id: number }) {
    try {
      const item = await this.repository.findOneOrFail(options.id, {
        relations: []
      });
      return { testing: item };
    } catch (error) {
      throw error;
    }
  }
  async findAll(options: {
    curPage: number;
    perPage: number;
    q?: string;
    sort?: string;
  }) {
    try {
      let objects: [Testing[], number];
      let qb = this.repository.createQueryBuilder('testing');
      if (options.q) {
        qb = qb.where('testing.name like :q or testing.id = :id', {
          q: `%${options.q}%`,
          id: +options.q
        });
      }
      options.sort =
        options.sort &&
        new Testing().hasOwnProperty(options.sort.replace('-', ''))
          ? options.sort
          : '-id';
      const field = options.sort.replace('-', '');
      if (options.sort)
        if (options.sort[0] === '-') {
          qb = qb.orderBy('testing.' + field, 'DESC');
        } else {
          qb = qb.orderBy('testing.' + field, 'ASC');
        }
      qb = qb
        .skip((options.curPage - 1) * options.perPage)
        .take(options.perPage);
      objects = await qb.getManyAndCount();
      return {
        testings: objects[0],
        meta: {
          perPage: options.perPage,
          totalPages:
            options.perPage > objects[1]
              ? 1
              : Math.ceil(objects[1] / options.perPage),
          totalResults: objects[1],
          curPage: options.curPage
        }
      };
    } catch (error) {
      throw error;
    }
  }
}
