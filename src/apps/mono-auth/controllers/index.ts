import { CodeVerifiesController } from './code-verifies.controller';
import { MobilesController } from './mobiles.controller';
import { TestingsController } from './testings.controller';

export const controllers = [
  CodeVerifiesController,
  MobilesController,
  TestingsController
];
