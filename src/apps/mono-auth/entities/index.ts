import { CodeVerify } from './code-verify.entity';
import { Mobile } from './mobile.entity';
import { Testing } from './testing.entity';

export const entities = [CodeVerify, Mobile, Testing];
