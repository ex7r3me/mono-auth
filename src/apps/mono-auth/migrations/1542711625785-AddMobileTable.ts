import { ContentType, Group, Permission } from '@mono-auth/core-nestjs';
import { Mobile } from '../entities/mobile.entity';
import { plainToClass } from 'class-transformer';
import { MigrationInterface, QueryRunner, Table } from 'typeorm';

export class AddMobileTable1542711625785 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<any> {
    // create table
    await queryRunner.createTable(
      new Table({
        name: 'mobile',
        columns: [
          {
            name: 'id',
            type: 'integer',
            isPrimary: true,
            isGenerated: true,
            generationStrategy: 'increment'
          },
          {
            name: 'name',
            type: 'varchar(100)',
            isNullable: false
          }
        ]
      }),
      true
    );
    // create/load content type
    const ctNewEntity = await queryRunner.manager
      .getRepository<ContentType>(ContentType)
      .save(plainToClass(ContentType, { name: 'mobile', title: 'Mobile' }));
    const ctUser = await queryRunner.manager
      .getRepository<ContentType>(ContentType)
      .findOneOrFail({
        where: {
          name: 'user'
        }
      });
    // create permissions
    const readPermissions = await queryRunner.manager
      .getRepository<Permission>(Permission)
      .save(
        plainToClass(Permission, [
          {
            title: 'Can read mobile',
            name: 'read_mobile',
            contentType: ctNewEntity
          },
          {
            title: 'Can read mobiles frame',
            name: 'read_mobiles-frame',
            contentType: ctUser
          },
          {
            title: 'Can read mobiles page',
            name: 'read_mobiles-page',
            contentType: ctUser
          }
        ])
      );
    const modifiPermissions = await queryRunner.manager
      .getRepository<Permission>(Permission)
      .save(
        plainToClass(Permission, [
          {
            title: 'Can add mobile',
            name: 'add_mobile',
            contentType: ctNewEntity
          },
          {
            title: 'Can change mobile',
            name: 'change_mobile',
            contentType: ctNewEntity
          },
          {
            title: 'Can delete mobile',
            name: 'delete_mobile',
            contentType: ctNewEntity
          }
        ])
      );
    // add permissions to groups
    const gUser = await queryRunner.manager
      .getRepository<Group>(Group)
      .findOneOrFail({
        where: {
          name: 'user'
        },
        relations: ['permissions']
      });
    const gAdmin = await queryRunner.manager
      .getRepository<Group>(Group)
      .findOneOrFail({
        where: {
          name: 'admin'
        },
        relations: ['permissions']
      });
    gUser.permissions = [...gUser.permissions, ...readPermissions];
    gAdmin.permissions = [
      ...gAdmin.permissions,
      ...readPermissions,
      ...modifiPermissions
    ];
    await queryRunner.manager.getRepository<Group>(Group).save([gUser, gAdmin]);
  }

  public async down(queryRunner: QueryRunner): Promise<any> {}
}
