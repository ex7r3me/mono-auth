import { ContentType, Group, Permission } from '@mono-auth/core-nestjs';
import { Testing } from '../entities/testing.entity';
import { plainToClass } from 'class-transformer';
import { MigrationInterface, QueryRunner, Table } from 'typeorm';

export class AddTestingTable1542706375020 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<any> {
    // create table
    await queryRunner.createTable(
      new Table({
        name: 'testing',
        columns: [
          {
            name: 'id',
            type: 'integer',
            isPrimary: true,
            isGenerated: true,
            generationStrategy: 'increment'
          },
          {
            name: 'name',
            type: 'varchar(100)',
            isNullable: false
          }
        ]
      }),
      true
    );
    // create/load content type
    const ctNewEntity = await queryRunner.manager
      .getRepository<ContentType>(ContentType)
      .save(plainToClass(ContentType, { name: 'testing', title: 'Testing' }));
    const ctUser = await queryRunner.manager
      .getRepository<ContentType>(ContentType)
      .findOneOrFail({
        where: {
          name: 'user'
        }
      });
    // create permissions
    const readPermissions = await queryRunner.manager
      .getRepository<Permission>(Permission)
      .save(
        plainToClass(Permission, [
          {
            title: 'Can read testing',
            name: 'read_testing',
            contentType: ctNewEntity
          },
          {
            title: 'Can read testings frame',
            name: 'read_testings-frame',
            contentType: ctUser
          },
          {
            title: 'Can read testings page',
            name: 'read_testings-page',
            contentType: ctUser
          }
        ])
      );
    const modifiPermissions = await queryRunner.manager
      .getRepository<Permission>(Permission)
      .save(
        plainToClass(Permission, [
          {
            title: 'Can add testing',
            name: 'add_testing',
            contentType: ctNewEntity
          },
          {
            title: 'Can change testing',
            name: 'change_testing',
            contentType: ctNewEntity
          },
          {
            title: 'Can delete testing',
            name: 'delete_testing',
            contentType: ctNewEntity
          }
        ])
      );
    // add permissions to groups
    const gUser = await queryRunner.manager
      .getRepository<Group>(Group)
      .findOneOrFail({
        where: {
          name: 'user'
        },
        relations: ['permissions']
      });
    const gAdmin = await queryRunner.manager
      .getRepository<Group>(Group)
      .findOneOrFail({
        where: {
          name: 'admin'
        },
        relations: ['permissions']
      });
    gUser.permissions = [...gUser.permissions, ...readPermissions];
    gAdmin.permissions = [
      ...gAdmin.permissions,
      ...readPermissions,
      ...modifiPermissions
    ];
    await queryRunner.manager.getRepository<Group>(Group).save([gUser, gAdmin]);
  }

  public async down(queryRunner: QueryRunner): Promise<any> {}
}
