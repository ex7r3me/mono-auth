import { ContentType, Group, Permission } from '@mono-auth/core-nestjs';
import { CodeVerify } from '../entities/code-verify.entity';
import { plainToClass } from 'class-transformer';
import { MigrationInterface, QueryRunner, Table } from 'typeorm';

export class AddCodeVerifyTable1542711634434 implements MigrationInterface {
  public async up(queryRunner: QueryRunner): Promise<any> {
    // create table
    await queryRunner.createTable(
      new Table({
        name: 'code_verify',
        columns: [
          {
            name: 'id',
            type: 'integer',
            isPrimary: true,
            isGenerated: true,
            generationStrategy: 'increment'
          },
          {
            name: 'name',
            type: 'varchar(100)',
            isNullable: false
          }
        ]
      }),
      true
    );
    // create/load content type
    const ctNewEntity = await queryRunner.manager
      .getRepository<ContentType>(ContentType)
      .save(
        plainToClass(ContentType, { name: 'code-verify', title: 'Code verify' })
      );
    const ctUser = await queryRunner.manager
      .getRepository<ContentType>(ContentType)
      .findOneOrFail({
        where: {
          name: 'user'
        }
      });
    // create permissions
    const readPermissions = await queryRunner.manager
      .getRepository<Permission>(Permission)
      .save(
        plainToClass(Permission, [
          {
            title: 'Can read code verify',
            name: 'read_code-verify',
            contentType: ctNewEntity
          },
          {
            title: 'Can read code verifies frame',
            name: 'read_code-verifies-frame',
            contentType: ctUser
          },
          {
            title: 'Can read code verifies page',
            name: 'read_code-verifies-page',
            contentType: ctUser
          }
        ])
      );
    const modifiPermissions = await queryRunner.manager
      .getRepository<Permission>(Permission)
      .save(
        plainToClass(Permission, [
          {
            title: 'Can add code verify',
            name: 'add_code-verify',
            contentType: ctNewEntity
          },
          {
            title: 'Can change code verify',
            name: 'change_code-verify',
            contentType: ctNewEntity
          },
          {
            title: 'Can delete code verify',
            name: 'delete_code-verify',
            contentType: ctNewEntity
          }
        ])
      );
    // add permissions to groups
    const gUser = await queryRunner.manager
      .getRepository<Group>(Group)
      .findOneOrFail({
        where: {
          name: 'user'
        },
        relations: ['permissions']
      });
    const gAdmin = await queryRunner.manager
      .getRepository<Group>(Group)
      .findOneOrFail({
        where: {
          name: 'admin'
        },
        relations: ['permissions']
      });
    gUser.permissions = [...gUser.permissions, ...readPermissions];
    gAdmin.permissions = [
      ...gAdmin.permissions,
      ...readPermissions,
      ...modifiPermissions
    ];
    await queryRunner.manager.getRepository<Group>(Group).save([gUser, gAdmin]);
  }

  public async down(queryRunner: QueryRunner): Promise<any> {}
}
