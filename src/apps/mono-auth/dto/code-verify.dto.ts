import { MaxLength } from 'class-validator';
import { ApiModelProperty } from '@nestjs/swagger';

export class CodeVerifyDto {
  @ApiModelProperty({ type: Number })
  id: number;
  @ApiModelProperty()
  @MaxLength(100)
  name: string;
}
