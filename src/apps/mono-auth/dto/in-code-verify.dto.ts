import { ApiModelProperty } from '@nestjs/swagger';
import { IsOptional, MaxLength } from 'class-validator';

export class InCodeVerifyDto {
  @IsOptional()
  id: number;
  @ApiModelProperty()
  @MaxLength(100)
  mobileNumber: string;
  @ApiModelProperty()
  code: string;
}
