import { ApiModelProperty } from '@nestjs/swagger';
import { Type } from 'class-transformer';
import { CodeVerifyDto } from './code-verify.dto';
export class OutCodeVerifyDto {
  @Type(() => CodeVerifyDto)
  @ApiModelProperty({ type: CodeVerifyDto })
  codeVerify: CodeVerifyDto;
}
