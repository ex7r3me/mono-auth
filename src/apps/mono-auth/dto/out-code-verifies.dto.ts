import { ApiModelProperty } from '@nestjs/swagger';
import { Type } from 'class-transformer';
import { CodeVerifyDto } from './code-verify.dto';
import { MetaDto } from '@mono-auth/core-nestjs';

export class OutCodeVerifiesDto {
  @Type(() => CodeVerifyDto)
  @ApiModelProperty({ type: CodeVerifyDto, isArray: true })
  codeVerifies: CodeVerifyDto[];
  @Type(() => MetaDto)
  @ApiModelProperty({ type: MetaDto })
  meta: MetaDto;
}
