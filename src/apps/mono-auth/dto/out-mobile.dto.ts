import { ApiModelProperty } from '@nestjs/swagger';
import { Type } from 'class-transformer';
import { MobileDto } from './mobile.dto';
export class OutMobileDto {
  @Type(() => MobileDto)
  @ApiModelProperty({ type: MobileDto })
  mobile: MobileDto;
}
