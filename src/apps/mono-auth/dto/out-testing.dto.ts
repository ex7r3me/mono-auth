import { ApiModelProperty } from '@nestjs/swagger';
import { Type } from 'class-transformer';
import { TestingDto } from './testing.dto';
export class OutTestingDto {
  @Type(() => TestingDto)
  @ApiModelProperty({ type: TestingDto })
  testing: TestingDto;
}
