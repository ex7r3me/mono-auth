import { ApiModelProperty } from '@nestjs/swagger';
import { Type } from 'class-transformer';
import { TestingDto } from './testing.dto';
import { MetaDto } from '@mono-auth/core-nestjs';

export class OutTestingsDto {
  @Type(() => TestingDto)
  @ApiModelProperty({ type: TestingDto, isArray: true })
  testings: TestingDto[];
  @Type(() => MetaDto)
  @ApiModelProperty({ type: MetaDto })
  meta: MetaDto;
}
