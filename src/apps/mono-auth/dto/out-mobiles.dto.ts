import { ApiModelProperty } from '@nestjs/swagger';
import { Type } from 'class-transformer';
import { MobileDto } from './mobile.dto';
import { MetaDto } from '@mono-auth/core-nestjs';

export class OutMobilesDto {
  @Type(() => MobileDto)
  @ApiModelProperty({ type: MobileDto, isArray: true })
  mobiles: MobileDto[];
  @Type(() => MetaDto)
  @ApiModelProperty({ type: MetaDto })
  meta: MetaDto;
}
