import { MaxLength } from 'class-validator';
import { ApiModelProperty } from '@nestjs/swagger';

export class MobileDto {
  @ApiModelProperty({ type: Number })
  id: number;
  @ApiModelProperty()
  @MaxLength(100)
  name: string;
}
