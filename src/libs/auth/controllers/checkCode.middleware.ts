import { Injectable, NestMiddleware, MiddlewareFunction } from '@nestjs/common';

@Injectable()
export class CheckCodeMiddleware implements NestMiddleware {
  resolve(...args: any[]): MiddlewareFunction {
    return (req, res, next) => {
      if (
        req.body.code === '1234' &&
        req.body.mobileNumber === '0090123456789'
      ) {
        next();
      } else {
        res.send({ message: 'wrong verification code' }, 401);
      }
    };
  }
}
