export const environment = {
  server: false,
  type: 'prod',
  production: true,
  apiUrl: '/api'
};
