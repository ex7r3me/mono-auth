export const environment = {
  server: true,
  type: 'prod',
  production: true,
  apiUrl: '/api'
};
