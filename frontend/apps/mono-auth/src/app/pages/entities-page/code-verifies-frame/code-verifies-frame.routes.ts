import { translate } from '@rucken/core';
import { CodeVerifiesFrameComponent } from './code-verifies-frame.component';
import { MetaGuard } from '@ngx-meta/core';

export const CodeVerifiesFrameRoutes = [
  {
    path: '',
    component: CodeVerifiesFrameComponent,
    canActivate: [MetaGuard],
    data: {
      name: 'code-verifies',
      meta: {
        title: translate('Code verifies'),
        description: translate('Code verifies frame')
      }
    }
  }
];
