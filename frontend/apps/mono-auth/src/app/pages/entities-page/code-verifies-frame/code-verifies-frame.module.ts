import { ModuleWithProviders, NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { CodeVerifiesGridModule } from '@mono-auth/web';
import { SharedModule } from '../../../shared/shared.module';
import { CodeVerifiesFrameComponent } from './code-verifies-frame.component';
import { CodeVerifiesFrameRoutes } from './code-verifies-frame.routes';
import { NgxPermissionsModule } from 'ngx-permissions';

@NgModule({
  imports: [
    SharedModule,
    NgxPermissionsModule,
    RouterModule.forChild(CodeVerifiesFrameRoutes),
    CodeVerifiesGridModule,
    FormsModule
  ],
  declarations: [
    CodeVerifiesFrameComponent
  ]
})
export class CodeVerifiesFrameModule {
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: CodeVerifiesFrameModule,
      providers: []
    };
  }
}
