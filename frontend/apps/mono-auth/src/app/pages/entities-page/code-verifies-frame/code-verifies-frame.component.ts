import { ChangeDetectionStrategy, Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { environment } from '../../../../environments/environment';

@Component({
  selector: 'code-verifies-frame',
  templateUrl: './code-verifies-frame.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CodeVerifiesFrameComponent {
  public apiUrl = environment.apiUrl;
  constructor(
    public activatedRoute: ActivatedRoute
  ) {
  }
}
