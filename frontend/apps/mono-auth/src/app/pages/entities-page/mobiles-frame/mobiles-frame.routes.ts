import { translate } from '@rucken/core';
import { MobilesFrameComponent } from './mobiles-frame.component';
import { MetaGuard } from '@ngx-meta/core';

export const MobilesFrameRoutes = [
  {
    path: '',
    component: MobilesFrameComponent,
    canActivate: [MetaGuard],
    data: {
      name: 'mobiles',
      meta: {
        title: translate('Mobiles'),
        description: translate('Mobiles frame')
      }
    }
  }
];
