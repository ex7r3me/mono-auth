import { ChangeDetectionStrategy, Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { environment } from '../../../../environments/environment';

@Component({
  selector: 'mobiles-frame',
  templateUrl: './mobiles-frame.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class MobilesFrameComponent {
  public apiUrl = environment.apiUrl;
  constructor(
    public activatedRoute: ActivatedRoute
  ) {
  }
}
