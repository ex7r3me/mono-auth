import { ModuleWithProviders, NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { MobilesGridModule } from '@mono-auth/web';
import { SharedModule } from '../../../shared/shared.module';
import { MobilesFrameComponent } from './mobiles-frame.component';
import { MobilesFrameRoutes } from './mobiles-frame.routes';
import { NgxPermissionsModule } from 'ngx-permissions';

@NgModule({
  imports: [
    SharedModule,
    NgxPermissionsModule,
    RouterModule.forChild(MobilesFrameRoutes),
    MobilesGridModule,
    FormsModule
  ],
  declarations: [
    MobilesFrameComponent
  ]
})
export class MobilesFrameModule {
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: MobilesFrameModule,
      providers: []
    };
  }
}
