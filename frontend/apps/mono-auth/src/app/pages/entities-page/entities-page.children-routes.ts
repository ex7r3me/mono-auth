import { CodeVerifiesFrameRoutes } from './code-verifies-frame/code-verifies-frame.routes';
import { ContentTypesFrameRoutes } from './content-types-frame/content-types-frame.routes';
import { GroupsFrameRoutes } from './groups-frame/groups-frame.routes';
import { MobilesFrameRoutes } from './mobiles-frame/mobiles-frame.routes';
import { PermissionsFrameRoutes } from './permissions-frame/permissions-frame.routes';
import { TestingsFrameRoutes } from './testings-frame/testings-frame.routes';
import { UsersFrameRoutes } from './users-frame/users-frame.routes';


export const EntitiesPageChildrenRoutes = [
  { path: '', redirectTo: '/entities/code-verifies', pathMatch: 'full' },
  {
    path: 'code-verifies',
    loadChildren: './code-verifies-frame/code-verifies-frame.module#CodeVerifiesFrameModule',
    data: CodeVerifiesFrameRoutes[0].data
  }, {
    path: 'content-types',
    loadChildren: './content-types-frame/content-types-frame.module#ContentTypesFrameModule',
    data: ContentTypesFrameRoutes[0].data
  }, {
    path: 'groups',
    loadChildren: './groups-frame/groups-frame.module#GroupsFrameModule',
    data: GroupsFrameRoutes[0].data
  }, {
    path: 'mobiles',
    loadChildren: './mobiles-frame/mobiles-frame.module#MobilesFrameModule',
    data: MobilesFrameRoutes[0].data
  }, {
    path: 'permissions',
    loadChildren: './permissions-frame/permissions-frame.module#PermissionsFrameModule',
    data: PermissionsFrameRoutes[0].data
  }, {
    path: 'testings',
    loadChildren: './testings-frame/testings-frame.module#TestingsFrameModule',
    data: TestingsFrameRoutes[0].data
  }, {
    path: 'users',
    loadChildren: './users-frame/users-frame.module#UsersFrameModule',
    data: UsersFrameRoutes[0].data
  }, 
];
