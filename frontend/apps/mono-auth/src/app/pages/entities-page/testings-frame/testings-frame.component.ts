import { ChangeDetectionStrategy, Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { environment } from '../../../../environments/environment';

@Component({
  selector: 'testings-frame',
  templateUrl: './testings-frame.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class TestingsFrameComponent {
  public apiUrl = environment.apiUrl;
  constructor(
    public activatedRoute: ActivatedRoute
  ) {
  }
}
