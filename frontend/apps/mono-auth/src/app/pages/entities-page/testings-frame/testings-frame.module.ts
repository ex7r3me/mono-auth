import { ModuleWithProviders, NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { TestingsGridModule } from '@mono-auth/web';
import { SharedModule } from '../../../shared/shared.module';
import { TestingsFrameComponent } from './testings-frame.component';
import { TestingsFrameRoutes } from './testings-frame.routes';
import { NgxPermissionsModule } from 'ngx-permissions';

@NgModule({
  imports: [
    SharedModule,
    NgxPermissionsModule,
    RouterModule.forChild(TestingsFrameRoutes),
    TestingsGridModule,
    FormsModule
  ],
  declarations: [
    TestingsFrameComponent
  ]
})
export class TestingsFrameModule {
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: TestingsFrameModule,
      providers: []
    };
  }
}
