import { translate } from '@rucken/core';
import { TestingsFrameComponent } from './testings-frame.component';
import { MetaGuard } from '@ngx-meta/core';

export const TestingsFrameRoutes = [
  {
    path: '',
    component: TestingsFrameComponent,
    canActivate: [MetaGuard],
    data: {
      name: 'testings',
      meta: {
        title: translate('Testings'),
        description: translate('Testings frame')
      }
    }
  }
];
