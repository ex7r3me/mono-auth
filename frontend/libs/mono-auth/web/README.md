[![Greenkeeper badge](https://badges.greenkeeper.io/mono-auth/core.svg)](https://greenkeeper.io/)
[![NPM version][npm-image]][npm-url]
[![Build Status][travis-image]][travis-url]
[![Gitter][gitter-image]][gitter-url]
[![Join the chat at telegram][telegram-image]][telegram-url]

Core with UI for web application maked on Angular7+

## What is this?

- **Core** - Base entities and services for create applications on `Angular7+.
- **Web** - Base ui grids and inputs and etc.

## Usage
```
git clone https://github.com/mono-auth/core.git my-app
cd my-app
npm install
npm run start:prod
```

## Quick links

[Live mono-auth](https://mono-auth.github.io/core) [[source]](https://github.com/mono-auth/core) - Mono auth application (backend: https://zeit.co/now).

[Live mono-auth (SSR)](https://mono-auth.herokuapp.com) [[source]](https://github.com/mono-auth/core) - Mono auth application with server side rendering (backend: https://zeit.co/now).

## License

MIT

[travis-image]: https://travis-ci.org/mono-auth/core.svg?branch=master
[travis-url]: https://travis-ci.org/mono-auth/core
[gitter-image]: https://img.shields.io/gitter/room/mono-auth/core.js.svg
[gitter-url]: https://gitter.im/mono-auth/core
[npm-image]: https://badge.fury.io/js/%40mono-auth%2Fweb.svg
[npm-url]: https://npmjs.org/package/@mono-auth/web
[dependencies-image]: https://david-dm.org/mono-auth/core/status.svg
[dependencies-url]: https://david-dm.org/mono-auth/core
[telegram-image]: https://img.shields.io/badge/chat-telegram-blue.svg?maxAge=2592000
[telegram-url]: https://t.me/mono-auth
