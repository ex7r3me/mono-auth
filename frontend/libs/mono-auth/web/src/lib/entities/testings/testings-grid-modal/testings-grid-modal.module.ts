import { CommonModule } from '@angular/common';
import { ModuleWithProviders, NgModule } from '@angular/core';
import { EntityGridModalModule } from '@rucken/web';
import { TestingsGridModule } from '../testings-grid/testings-grid.module';
import { TestingsGridModalComponent } from './testings-grid-modal.component';

@NgModule({
  imports: [
    CommonModule,
    EntityGridModalModule,
    TestingsGridModule
  ],
  declarations: [
    TestingsGridModalComponent
  ],
  entryComponents: [
    TestingsGridModalComponent
  ],
  exports: [
    TestingsGridModalComponent,
    EntityGridModalModule,
    TestingsGridModule
  ]
})
export class TestingsGridModalModule {
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: TestingsGridModalModule,
      providers: []
  };
}
}
