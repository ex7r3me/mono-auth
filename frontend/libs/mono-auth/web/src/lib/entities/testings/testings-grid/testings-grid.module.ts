import { CommonModule } from '@angular/common';
import { ModuleWithProviders, NgModule } from '@angular/core';
import { EntityGridModule } from '@rucken/web';
import { TestingModalModule } from '../testing-modal/testing-modal.module';
import { TestingsGridComponent } from './testings-grid.component';

@NgModule({
  imports: [
    CommonModule,
    EntityGridModule,
    TestingModalModule
  ],
  declarations: [
    TestingsGridComponent
  ],
  exports: [
    TestingsGridComponent,
    EntityGridModule,
    TestingModalModule
  ]
})
export class TestingsGridModule {
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: TestingsGridModule,
      providers: []
    };
  }
}
