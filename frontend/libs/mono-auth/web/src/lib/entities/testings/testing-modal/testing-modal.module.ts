import { CommonModule } from '@angular/common';
import { ModuleWithProviders, NgModule } from '@angular/core';
import { PromptFormModalModule } from '@rucken/web';
import { TestingModalComponent } from './testing-modal.component';

@NgModule({
  imports: [
    CommonModule,
    PromptFormModalModule
  ],
  declarations: [
    TestingModalComponent
  ],
  entryComponents: [
    TestingModalComponent
  ],
  exports: [
    TestingModalComponent,
    PromptFormModalModule
  ]
})
export class TestingModalModule {
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: TestingModalModule,
      providers: []
  };
}
}
