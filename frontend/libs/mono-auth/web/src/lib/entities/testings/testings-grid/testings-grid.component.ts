import { ChangeDetectionStrategy, Inject, Component, OnInit, Input } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { ErrorsExtractor, translate } from '@rucken/core';
import { Testing, TESTINGS_CONFIG_TOKEN } from '@mono-auth/core';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { DynamicRepository, IRestProviderOptions } from 'ngx-repository';
import { BaseEntityListComponent, IBaseEntityModalOptions } from '@rucken/web';
import { MessageModalService } from '@rucken/web';
import { TestingModalComponent } from '../testing-modal/testing-modal.component';


@Component({
  selector: 'testings-grid',
  templateUrl: './testings-grid.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class TestingsGridComponent extends BaseEntityListComponent <Testing> implements OnInit {
  @Input()
  modalItem: IBaseEntityModalOptions = {
      component: TestingModalComponent
  };
  @Input()
  title = translate('Testings');
  constructor(
    public modalService: BsModalService,
    protected errorsExtractor: ErrorsExtractor,
    protected translateService: TranslateService,
    protected dynamicRepository: DynamicRepository,
    protected messageModalService: MessageModalService,
    @Inject(TESTINGS_CONFIG_TOKEN) protected testingsConfig: IRestProviderOptions<Testing>
  ) {
    super(
      dynamicRepository.fork <Testing> (Testing),
      modalService,
      Testing
    );
  }
  ngOnInit() {
    if (!this.mockedItems) {
      this.useRest({
        apiUrl: this.apiUrl,
        ...this.testingsConfig
      });
    }
    if (this.mockedItems) {
      this.useMock({
        items: this.mockedItems,
        ...this.testingsConfig
      });
    }
  }
  /*
  createDeleteModal(item: Testing): BsModalRef {
    return this.modalService.show(TestingModalComponent, {
      class: 'modal-md',
      initialState: {
        title: this.strings.deleteTitle,
        message: this.strings.deleteMessage,
        yesTitle: translate('Delete'),
        data: item,
        apiUrl: this.apiUrl
      }
    });
  }
  createCreateModal(): BsModalRef {
    const item = new Testing();
    return this.modalService.show(TestingModalComponent, {
      class: 'modal-md',
      initialState: {
        title: this.strings.createTitle,
        yesTitle: translate('Create'),
        data: item,
        apiUrl: this.apiUrl
      }
    });
  }
  createUpdateModal(item?: Testing): BsModalRef {
    return this.modalService.show(TestingModalComponent, {
      class: 'modal-md',
      initialState: {
        title: this.strings.updateTitle,
        yesTitle: translate('Save'),
        data: item,
        apiUrl: this.apiUrl
      }
    });
  }
  createViewModal(item?: Testing): BsModalRef {
    return this.modalService.show(TestingModalComponent, {
      class: 'modal-md',
      initialState: {
        title: this.strings.viewTitle,
        noTitle: translate('Close'),
        readonly: true,
        data: item,
        apiUrl: this.apiUrl
      }
    });
  }
  */
}
