import { CommonModule } from '@angular/common';
import { ModuleWithProviders, NgModule } from '@angular/core';
import { EntitySelectModule } from '@rucken/web';
import { TestingsGridModalModule } from '../testings-grid-modal/testings-grid-modal.module';
import { TestingSelectComponent } from './testing-select.component';

@NgModule({
  imports: [
    CommonModule,
    EntitySelectModule,
    TestingsGridModalModule
  ],
  declarations: [
    TestingSelectComponent
  ],
  exports: [
    TestingSelectComponent,
    EntitySelectModule,
    TestingsGridModalModule
  ]
})
export class TestingSelectModule {
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: TestingSelectModule,
      providers: []
  };
}
}
