import { Component, Input, ViewChild, ChangeDetectionStrategy } from '@angular/core';
import { Testing } from '@mono-auth/core';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { BaseEntityListModalComponent } from '@rucken/web';
import { TestingsGridComponent } from '../testings-grid/testings-grid.component';

@Component({
  selector: 'testings-grid-modal',
  templateUrl: './testings-grid-modal.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class TestingsGridModalComponent extends BaseEntityListModalComponent <Testing> {

  @ViewChild('grid')
  grid: TestingsGridComponent;
  @Input()
  apiUrl?: string;

  constructor(
    protected bsModalRef: BsModalRef
  ) {
    super(bsModalRef);
  }
}
