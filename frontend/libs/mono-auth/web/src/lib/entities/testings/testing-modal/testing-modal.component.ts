import { Component, ChangeDetectionStrategy } from '@angular/core';
import { Testing } from '@mono-auth/core';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { BasePromptFormModalComponent } from '@rucken/web';

@Component({
  selector: 'testing-modal',
  templateUrl: './testing-modal.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class TestingModalComponent extends BasePromptFormModalComponent <Testing> {

  constructor(
    protected bsModalRef: BsModalRef
  ) {
    super(bsModalRef);
    this.group(Testing);
  }
}
