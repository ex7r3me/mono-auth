import { Component, EventEmitter, OnInit, Output, Input, Inject, ChangeDetectionStrategy } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { ErrorsExtractor, translate } from '@rucken/core';
import { Testing, TESTINGS_CONFIG_TOKEN } from '@mono-auth/core';
import { BsModalService } from 'ngx-bootstrap/modal';
import { DynamicRepository, IRestProviderOptions } from 'ngx-repository';
import { MessageModalService, IBaseEntityModalOptions } from '@rucken/web';
import { TestingsGridModalComponent } from '../testings-grid-modal/testings-grid-modal.component';
import { TestingsGridComponent } from '../testings-grid/testings-grid.component';


@Component({
  selector: 'testing-input',
  templateUrl: './testing-input.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class TestingInputComponent extends TestingsGridComponent implements OnInit {

  @Output()
  select = new EventEmitter <Testing> ();
  @Input()
  modalAppendFromGrid: IBaseEntityModalOptions = {
    component: TestingsGridModalComponent,
    initialState: {
      title: translate('Select testing'),
      yesTitle: translate('Select')
    }
  };

  constructor(
    public modalService: BsModalService,
    protected errorsExtractor: ErrorsExtractor,
    protected translateService: TranslateService,
    protected dynamicRepository: DynamicRepository,
    protected messageModalService: MessageModalService,
    @Inject(TESTINGS_CONFIG_TOKEN) protected testingsConfig: IRestProviderOptions<Testing>
  ) {
    super(
      modalService,
      errorsExtractor,
      translateService,
      dynamicRepository,
      messageModalService,
      testingsConfig
    );
  }
  ngOnInit() {
    this.mockedItems = [];
    this.useMock({
      items: this.mockedItems,
      ...this.testingsConfig
    });
    this.mockedItemsChange.subscribe(items =>
      this.onSelect(items[0])
    );
  }
  onSelect(item: Testing) {
    this.select.emit(item);
  }
}
