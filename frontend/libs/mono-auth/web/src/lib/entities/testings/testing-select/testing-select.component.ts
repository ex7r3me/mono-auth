import { Component, Input, OnInit, Inject, ChangeDetectionStrategy } from '@angular/core';
import { FormControl } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';
import { ErrorsExtractor } from '@rucken/core';
import { Testing, TESTINGS_CONFIG_TOKEN } from '@mono-auth/core';
import { BsModalService } from 'ngx-bootstrap/modal';
import { DynamicRepository, IRestProviderOptions } from 'ngx-repository';
import { MessageModalService } from '@rucken/web';
import { TestingsGridComponent } from '../testings-grid/testings-grid.component';


@Component({
  selector: 'testing-select',
  templateUrl: './testing-select.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class TestingSelectComponent extends TestingsGridComponent implements OnInit {

  @Input()
  searchField: FormControl = new FormControl();

  nameField = 'name';

  constructor(
    public modalService: BsModalService,
    protected errorsExtractor: ErrorsExtractor,
    protected translateService: TranslateService,
    protected dynamicRepository: DynamicRepository,
    protected messageModalService: MessageModalService,
    @Inject(TESTINGS_CONFIG_TOKEN) protected testingsConfig: IRestProviderOptions<Testing>
  ) {
    super(
      modalService,
      errorsExtractor,
      translateService,
      dynamicRepository,
      messageModalService,
      testingsConfig
    );
  }
  ngOnInit() {
    if (!this.mockedItems) {
      this.useRest({
        apiUrl: this.apiUrl,
        ...this.testingsConfig,
        paginationMeta: { perPage: 1000 }
      });
    }
    if (this.mockedItems) {
      this.useMock({
        items: this.mockedItems,
        ...this.testingsConfig
      });
    }
  }
  checkChange(value: any, item: any) {
    return item instanceof Testing;
  }
}
