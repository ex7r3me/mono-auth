import { CommonModule } from '@angular/common';
import { ModuleWithProviders, NgModule } from '@angular/core';
import { EntityInputModule } from '@rucken/web';
import { TestingsGridModalModule } from '../testings-grid-modal/testings-grid-modal.module';
import { TestingInputComponent } from './testing-input.component';

@NgModule({
  imports: [
    CommonModule,
    EntityInputModule,
    TestingsGridModalModule
  ],
  declarations: [
    TestingInputComponent
  ],
  exports: [
    TestingInputComponent,
    EntityInputModule,
    TestingsGridModalModule
  ]
})
export class TestingInputModule {
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: TestingInputModule,
      providers: []
  };
}
}
