import { Component, Input, OnInit, Inject, ChangeDetectionStrategy } from '@angular/core';
import { FormControl } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';
import { ErrorsExtractor } from '@rucken/core';
import { Mobile, MOBILES_CONFIG_TOKEN } from '@mono-auth/core';
import { BsModalService } from 'ngx-bootstrap/modal';
import { DynamicRepository, IRestProviderOptions } from 'ngx-repository';
import { MessageModalService } from '@rucken/web';
import { MobilesGridComponent } from '../mobiles-grid/mobiles-grid.component';


@Component({
  selector: 'mobile-select',
  templateUrl: './mobile-select.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class MobileSelectComponent extends MobilesGridComponent implements OnInit {

  @Input()
  searchField: FormControl = new FormControl();

  nameField = 'name';

  constructor(
    public modalService: BsModalService,
    protected errorsExtractor: ErrorsExtractor,
    protected translateService: TranslateService,
    protected dynamicRepository: DynamicRepository,
    protected messageModalService: MessageModalService,
    @Inject(MOBILES_CONFIG_TOKEN) protected mobilesConfig: IRestProviderOptions<Mobile>
  ) {
    super(
      modalService,
      errorsExtractor,
      translateService,
      dynamicRepository,
      messageModalService,
      mobilesConfig
    );
  }
  ngOnInit() {
    if (!this.mockedItems) {
      this.useRest({
        apiUrl: this.apiUrl,
        ...this.mobilesConfig,
        paginationMeta: { perPage: 1000 }
      });
    }
    if (this.mockedItems) {
      this.useMock({
        items: this.mockedItems,
        ...this.mobilesConfig
      });
    }
  }
  checkChange(value: any, item: any) {
    return item instanceof Mobile;
  }
}
