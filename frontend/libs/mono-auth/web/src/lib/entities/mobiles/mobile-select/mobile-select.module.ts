import { CommonModule } from '@angular/common';
import { ModuleWithProviders, NgModule } from '@angular/core';
import { EntitySelectModule } from '@rucken/web';
import { MobilesGridModalModule } from '../mobiles-grid-modal/mobiles-grid-modal.module';
import { MobileSelectComponent } from './mobile-select.component';

@NgModule({
  imports: [
    CommonModule,
    EntitySelectModule,
    MobilesGridModalModule
  ],
  declarations: [
    MobileSelectComponent
  ],
  exports: [
    MobileSelectComponent,
    EntitySelectModule,
    MobilesGridModalModule
  ]
})
export class MobileSelectModule {
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: MobileSelectModule,
      providers: []
  };
}
}
