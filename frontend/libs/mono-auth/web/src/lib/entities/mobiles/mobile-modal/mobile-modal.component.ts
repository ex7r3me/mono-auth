import { Component, ChangeDetectionStrategy } from '@angular/core';
import { Mobile } from '@mono-auth/core';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { BasePromptFormModalComponent } from '@rucken/web';

@Component({
  selector: 'mobile-modal',
  templateUrl: './mobile-modal.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class MobileModalComponent extends BasePromptFormModalComponent <Mobile> {

  constructor(
    protected bsModalRef: BsModalRef
  ) {
    super(bsModalRef);
    this.group(Mobile);
  }
}
