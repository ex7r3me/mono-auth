import { CommonModule } from '@angular/common';
import { ModuleWithProviders, NgModule } from '@angular/core';
import { EntityGridModule } from '@rucken/web';
import { MobileModalModule } from '../mobile-modal/mobile-modal.module';
import { MobilesGridComponent } from './mobiles-grid.component';

@NgModule({
  imports: [
    CommonModule,
    EntityGridModule,
    MobileModalModule
  ],
  declarations: [
    MobilesGridComponent
  ],
  exports: [
    MobilesGridComponent,
    EntityGridModule,
    MobileModalModule
  ]
})
export class MobilesGridModule {
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: MobilesGridModule,
      providers: []
    };
  }
}
