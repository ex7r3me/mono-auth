import { ChangeDetectionStrategy, Inject, Component, OnInit, Input } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { ErrorsExtractor, translate } from '@rucken/core';
import { Mobile, MOBILES_CONFIG_TOKEN } from '@mono-auth/core';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { DynamicRepository, IRestProviderOptions } from 'ngx-repository';
import { BaseEntityListComponent, IBaseEntityModalOptions } from '@rucken/web';
import { MessageModalService } from '@rucken/web';
import { MobileModalComponent } from '../mobile-modal/mobile-modal.component';


@Component({
  selector: 'mobiles-grid',
  templateUrl: './mobiles-grid.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class MobilesGridComponent extends BaseEntityListComponent <Mobile> implements OnInit {
  @Input()
  modalItem: IBaseEntityModalOptions = {
      component: MobileModalComponent
  };
  @Input()
  title = translate('Mobiles');
  constructor(
    public modalService: BsModalService,
    protected errorsExtractor: ErrorsExtractor,
    protected translateService: TranslateService,
    protected dynamicRepository: DynamicRepository,
    protected messageModalService: MessageModalService,
    @Inject(MOBILES_CONFIG_TOKEN) protected mobilesConfig: IRestProviderOptions<Mobile>
  ) {
    super(
      dynamicRepository.fork <Mobile> (Mobile),
      modalService,
      Mobile
    );
  }
  ngOnInit() {
    if (!this.mockedItems) {
      this.useRest({
        apiUrl: this.apiUrl,
        ...this.mobilesConfig
      });
    }
    if (this.mockedItems) {
      this.useMock({
        items: this.mockedItems,
        ...this.mobilesConfig
      });
    }
  }
  /*
  createDeleteModal(item: Mobile): BsModalRef {
    return this.modalService.show(MobileModalComponent, {
      class: 'modal-md',
      initialState: {
        title: this.strings.deleteTitle,
        message: this.strings.deleteMessage,
        yesTitle: translate('Delete'),
        data: item,
        apiUrl: this.apiUrl
      }
    });
  }
  createCreateModal(): BsModalRef {
    const item = new Mobile();
    return this.modalService.show(MobileModalComponent, {
      class: 'modal-md',
      initialState: {
        title: this.strings.createTitle,
        yesTitle: translate('Create'),
        data: item,
        apiUrl: this.apiUrl
      }
    });
  }
  createUpdateModal(item?: Mobile): BsModalRef {
    return this.modalService.show(MobileModalComponent, {
      class: 'modal-md',
      initialState: {
        title: this.strings.updateTitle,
        yesTitle: translate('Save'),
        data: item,
        apiUrl: this.apiUrl
      }
    });
  }
  createViewModal(item?: Mobile): BsModalRef {
    return this.modalService.show(MobileModalComponent, {
      class: 'modal-md',
      initialState: {
        title: this.strings.viewTitle,
        noTitle: translate('Close'),
        readonly: true,
        data: item,
        apiUrl: this.apiUrl
      }
    });
  }
  */
}
