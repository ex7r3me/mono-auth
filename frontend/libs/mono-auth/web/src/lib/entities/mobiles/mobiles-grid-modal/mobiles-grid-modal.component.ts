import { Component, Input, ViewChild, ChangeDetectionStrategy } from '@angular/core';
import { Mobile } from '@mono-auth/core';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { BaseEntityListModalComponent } from '@rucken/web';
import { MobilesGridComponent } from '../mobiles-grid/mobiles-grid.component';

@Component({
  selector: 'mobiles-grid-modal',
  templateUrl: './mobiles-grid-modal.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class MobilesGridModalComponent extends BaseEntityListModalComponent <Mobile> {

  @ViewChild('grid')
  grid: MobilesGridComponent;
  @Input()
  apiUrl?: string;

  constructor(
    protected bsModalRef: BsModalRef
  ) {
    super(bsModalRef);
  }
}
