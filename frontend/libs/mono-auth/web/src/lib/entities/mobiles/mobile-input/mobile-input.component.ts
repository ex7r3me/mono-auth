import { Component, EventEmitter, OnInit, Output, Input, Inject, ChangeDetectionStrategy } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { ErrorsExtractor, translate } from '@rucken/core';
import { Mobile, MOBILES_CONFIG_TOKEN } from '@mono-auth/core';
import { BsModalService } from 'ngx-bootstrap/modal';
import { DynamicRepository, IRestProviderOptions } from 'ngx-repository';
import { MessageModalService, IBaseEntityModalOptions } from '@rucken/web';
import { MobilesGridModalComponent } from '../mobiles-grid-modal/mobiles-grid-modal.component';
import { MobilesGridComponent } from '../mobiles-grid/mobiles-grid.component';


@Component({
  selector: 'mobile-input',
  templateUrl: './mobile-input.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class MobileInputComponent extends MobilesGridComponent implements OnInit {

  @Output()
  select = new EventEmitter <Mobile> ();
  @Input()
  modalAppendFromGrid: IBaseEntityModalOptions = {
    component: MobilesGridModalComponent,
    initialState: {
      title: translate('Select mobile'),
      yesTitle: translate('Select')
    }
  };

  constructor(
    public modalService: BsModalService,
    protected errorsExtractor: ErrorsExtractor,
    protected translateService: TranslateService,
    protected dynamicRepository: DynamicRepository,
    protected messageModalService: MessageModalService,
    @Inject(MOBILES_CONFIG_TOKEN) protected mobilesConfig: IRestProviderOptions<Mobile>
  ) {
    super(
      modalService,
      errorsExtractor,
      translateService,
      dynamicRepository,
      messageModalService,
      mobilesConfig
    );
  }
  ngOnInit() {
    this.mockedItems = [];
    this.useMock({
      items: this.mockedItems,
      ...this.mobilesConfig
    });
    this.mockedItemsChange.subscribe(items =>
      this.onSelect(items[0])
    );
  }
  onSelect(item: Mobile) {
    this.select.emit(item);
  }
}
