import { CommonModule } from '@angular/common';
import { ModuleWithProviders, NgModule } from '@angular/core';
import { PromptFormModalModule } from '@rucken/web';
import { MobileModalComponent } from './mobile-modal.component';

@NgModule({
  imports: [
    CommonModule,
    PromptFormModalModule
  ],
  declarations: [
    MobileModalComponent
  ],
  entryComponents: [
    MobileModalComponent
  ],
  exports: [
    MobileModalComponent,
    PromptFormModalModule
  ]
})
export class MobileModalModule {
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: MobileModalModule,
      providers: []
  };
}
}
