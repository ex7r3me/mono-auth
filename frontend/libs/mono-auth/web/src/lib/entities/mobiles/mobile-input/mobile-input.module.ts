import { CommonModule } from '@angular/common';
import { ModuleWithProviders, NgModule } from '@angular/core';
import { EntityInputModule } from '@rucken/web';
import { MobilesGridModalModule } from '../mobiles-grid-modal/mobiles-grid-modal.module';
import { MobileInputComponent } from './mobile-input.component';

@NgModule({
  imports: [
    CommonModule,
    EntityInputModule,
    MobilesGridModalModule
  ],
  declarations: [
    MobileInputComponent
  ],
  exports: [
    MobileInputComponent,
    EntityInputModule,
    MobilesGridModalModule
  ]
})
export class MobileInputModule {
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: MobileInputModule,
      providers: []
  };
}
}
