import { CommonModule } from '@angular/common';
import { ModuleWithProviders, NgModule } from '@angular/core';
import { EntityGridModalModule } from '@rucken/web';
import { MobilesGridModule } from '../mobiles-grid/mobiles-grid.module';
import { MobilesGridModalComponent } from './mobiles-grid-modal.component';

@NgModule({
  imports: [
    CommonModule,
    EntityGridModalModule,
    MobilesGridModule
  ],
  declarations: [
    MobilesGridModalComponent
  ],
  entryComponents: [
    MobilesGridModalComponent
  ],
  exports: [
    MobilesGridModalComponent,
    EntityGridModalModule,
    MobilesGridModule
  ]
})
export class MobilesGridModalModule {
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: MobilesGridModalModule,
      providers: []
  };
}
}
