import { Component, Input, ViewChild, ChangeDetectionStrategy } from '@angular/core';
import { CodeVerify } from '@mono-auth/core';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { BaseEntityListModalComponent } from '@rucken/web';
import { CodeVerifiesGridComponent } from '../code-verifies-grid/code-verifies-grid.component';

@Component({
  selector: 'code-verifies-grid-modal',
  templateUrl: './code-verifies-grid-modal.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CodeVerifiesGridModalComponent extends BaseEntityListModalComponent <CodeVerify> {

  @ViewChild('grid')
  grid: CodeVerifiesGridComponent;
  @Input()
  apiUrl?: string;

  constructor(
    protected bsModalRef: BsModalRef
  ) {
    super(bsModalRef);
  }
}
