import { Component, ChangeDetectionStrategy } from '@angular/core';
import { CodeVerify } from '@mono-auth/core';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { BasePromptFormModalComponent } from '@rucken/web';

@Component({
  selector: 'code-verify-modal',
  templateUrl: './code-verify-modal.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CodeVerifyModalComponent extends BasePromptFormModalComponent <CodeVerify> {

  constructor(
    protected bsModalRef: BsModalRef
  ) {
    super(bsModalRef);
    this.group(CodeVerify);
  }
}
