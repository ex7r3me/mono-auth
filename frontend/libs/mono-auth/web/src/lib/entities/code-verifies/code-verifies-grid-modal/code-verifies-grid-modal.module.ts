import { CommonModule } from '@angular/common';
import { ModuleWithProviders, NgModule } from '@angular/core';
import { EntityGridModalModule } from '@rucken/web';
import { CodeVerifiesGridModule } from '../code-verifies-grid/code-verifies-grid.module';
import { CodeVerifiesGridModalComponent } from './code-verifies-grid-modal.component';

@NgModule({
  imports: [
    CommonModule,
    EntityGridModalModule,
    CodeVerifiesGridModule
  ],
  declarations: [
    CodeVerifiesGridModalComponent
  ],
  entryComponents: [
    CodeVerifiesGridModalComponent
  ],
  exports: [
    CodeVerifiesGridModalComponent,
    EntityGridModalModule,
    CodeVerifiesGridModule
  ]
})
export class CodeVerifiesGridModalModule {
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: CodeVerifiesGridModalModule,
      providers: []
  };
}
}
