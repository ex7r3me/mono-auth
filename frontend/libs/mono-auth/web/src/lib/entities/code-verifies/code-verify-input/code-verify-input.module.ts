import { CommonModule } from '@angular/common';
import { ModuleWithProviders, NgModule } from '@angular/core';
import { EntityInputModule } from '@rucken/web';
import { CodeVerifiesGridModalModule } from '../code-verifies-grid-modal/code-verifies-grid-modal.module';
import { CodeVerifyInputComponent } from './code-verify-input.component';

@NgModule({
  imports: [
    CommonModule,
    EntityInputModule,
    CodeVerifiesGridModalModule
  ],
  declarations: [
    CodeVerifyInputComponent
  ],
  exports: [
    CodeVerifyInputComponent,
    EntityInputModule,
    CodeVerifiesGridModalModule
  ]
})
export class CodeVerifyInputModule {
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: CodeVerifyInputModule,
      providers: []
  };
}
}
