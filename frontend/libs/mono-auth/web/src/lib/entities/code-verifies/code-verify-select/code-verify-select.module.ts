import { CommonModule } from '@angular/common';
import { ModuleWithProviders, NgModule } from '@angular/core';
import { EntitySelectModule } from '@rucken/web';
import { CodeVerifiesGridModalModule } from '../code-verifies-grid-modal/code-verifies-grid-modal.module';
import { CodeVerifySelectComponent } from './code-verify-select.component';

@NgModule({
  imports: [
    CommonModule,
    EntitySelectModule,
    CodeVerifiesGridModalModule
  ],
  declarations: [
    CodeVerifySelectComponent
  ],
  exports: [
    CodeVerifySelectComponent,
    EntitySelectModule,
    CodeVerifiesGridModalModule
  ]
})
export class CodeVerifySelectModule {
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: CodeVerifySelectModule,
      providers: []
  };
}
}
