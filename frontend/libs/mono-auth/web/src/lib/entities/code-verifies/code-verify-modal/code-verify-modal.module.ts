import { CommonModule } from '@angular/common';
import { ModuleWithProviders, NgModule } from '@angular/core';
import { PromptFormModalModule } from '@rucken/web';
import { CodeVerifyModalComponent } from './code-verify-modal.component';

@NgModule({
  imports: [
    CommonModule,
    PromptFormModalModule
  ],
  declarations: [
    CodeVerifyModalComponent
  ],
  entryComponents: [
    CodeVerifyModalComponent
  ],
  exports: [
    CodeVerifyModalComponent,
    PromptFormModalModule
  ]
})
export class CodeVerifyModalModule {
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: CodeVerifyModalModule,
      providers: []
  };
}
}
