import { CommonModule } from '@angular/common';
import { ModuleWithProviders, NgModule } from '@angular/core';
import { EntityGridModule } from '@rucken/web';
import { CodeVerifyModalModule } from '../code-verify-modal/code-verify-modal.module';
import { CodeVerifiesGridComponent } from './code-verifies-grid.component';

@NgModule({
  imports: [
    CommonModule,
    EntityGridModule,
    CodeVerifyModalModule
  ],
  declarations: [
    CodeVerifiesGridComponent
  ],
  exports: [
    CodeVerifiesGridComponent,
    EntityGridModule,
    CodeVerifyModalModule
  ]
})
export class CodeVerifiesGridModule {
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: CodeVerifiesGridModule,
      providers: []
    };
  }
}
