import { ChangeDetectionStrategy, Inject, Component, OnInit, Input } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { ErrorsExtractor, translate } from '@rucken/core';
import { CodeVerify, CODE_VERIFIES_CONFIG_TOKEN } from '@mono-auth/core';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { DynamicRepository, IRestProviderOptions } from 'ngx-repository';
import { BaseEntityListComponent, IBaseEntityModalOptions } from '@rucken/web';
import { MessageModalService } from '@rucken/web';
import { CodeVerifyModalComponent } from '../code-verify-modal/code-verify-modal.component';


@Component({
  selector: 'code-verifies-grid',
  templateUrl: './code-verifies-grid.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CodeVerifiesGridComponent extends BaseEntityListComponent <CodeVerify> implements OnInit {
  @Input()
  modalItem: IBaseEntityModalOptions = {
      component: CodeVerifyModalComponent
  };
  @Input()
  title = translate('Code verifies');
  constructor(
    public modalService: BsModalService,
    protected errorsExtractor: ErrorsExtractor,
    protected translateService: TranslateService,
    protected dynamicRepository: DynamicRepository,
    protected messageModalService: MessageModalService,
    @Inject(CODE_VERIFIES_CONFIG_TOKEN) protected codeVerifiesConfig: IRestProviderOptions<CodeVerify>
  ) {
    super(
      dynamicRepository.fork <CodeVerify> (CodeVerify),
      modalService,
      CodeVerify
    );
  }
  ngOnInit() {
    if (!this.mockedItems) {
      this.useRest({
        apiUrl: this.apiUrl,
        ...this.codeVerifiesConfig
      });
    }
    if (this.mockedItems) {
      this.useMock({
        items: this.mockedItems,
        ...this.codeVerifiesConfig
      });
    }
  }
  /*
  createDeleteModal(item: CodeVerify): BsModalRef {
    return this.modalService.show(CodeVerifyModalComponent, {
      class: 'modal-md',
      initialState: {
        title: this.strings.deleteTitle,
        message: this.strings.deleteMessage,
        yesTitle: translate('Delete'),
        data: item,
        apiUrl: this.apiUrl
      }
    });
  }
  createCreateModal(): BsModalRef {
    const item = new CodeVerify();
    return this.modalService.show(CodeVerifyModalComponent, {
      class: 'modal-md',
      initialState: {
        title: this.strings.createTitle,
        yesTitle: translate('Create'),
        data: item,
        apiUrl: this.apiUrl
      }
    });
  }
  createUpdateModal(item?: CodeVerify): BsModalRef {
    return this.modalService.show(CodeVerifyModalComponent, {
      class: 'modal-md',
      initialState: {
        title: this.strings.updateTitle,
        yesTitle: translate('Save'),
        data: item,
        apiUrl: this.apiUrl
      }
    });
  }
  createViewModal(item?: CodeVerify): BsModalRef {
    return this.modalService.show(CodeVerifyModalComponent, {
      class: 'modal-md',
      initialState: {
        title: this.strings.viewTitle,
        noTitle: translate('Close'),
        readonly: true,
        data: item,
        apiUrl: this.apiUrl
      }
    });
  }
  */
}
