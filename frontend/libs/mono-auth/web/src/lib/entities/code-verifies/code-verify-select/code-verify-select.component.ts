import { Component, Input, OnInit, Inject, ChangeDetectionStrategy } from '@angular/core';
import { FormControl } from '@angular/forms';
import { TranslateService } from '@ngx-translate/core';
import { ErrorsExtractor } from '@rucken/core';
import { CodeVerify, CODE_VERIFIES_CONFIG_TOKEN } from '@mono-auth/core';
import { BsModalService } from 'ngx-bootstrap/modal';
import { DynamicRepository, IRestProviderOptions } from 'ngx-repository';
import { MessageModalService } from '@rucken/web';
import { CodeVerifiesGridComponent } from '../code-verifies-grid/code-verifies-grid.component';


@Component({
  selector: 'code-verify-select',
  templateUrl: './code-verify-select.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CodeVerifySelectComponent extends CodeVerifiesGridComponent implements OnInit {

  @Input()
  searchField: FormControl = new FormControl();

  nameField = 'name';

  constructor(
    public modalService: BsModalService,
    protected errorsExtractor: ErrorsExtractor,
    protected translateService: TranslateService,
    protected dynamicRepository: DynamicRepository,
    protected messageModalService: MessageModalService,
    @Inject(CODE_VERIFIES_CONFIG_TOKEN) protected codeVerifiesConfig: IRestProviderOptions<CodeVerify>
  ) {
    super(
      modalService,
      errorsExtractor,
      translateService,
      dynamicRepository,
      messageModalService,
      codeVerifiesConfig
    );
  }
  ngOnInit() {
    if (!this.mockedItems) {
      this.useRest({
        apiUrl: this.apiUrl,
        ...this.codeVerifiesConfig,
        paginationMeta: { perPage: 1000 }
      });
    }
    if (this.mockedItems) {
      this.useMock({
        items: this.mockedItems,
        ...this.codeVerifiesConfig
      });
    }
  }
  checkChange(value: any, item: any) {
    return item instanceof CodeVerify;
  }
}
