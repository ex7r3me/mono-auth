import { Component, EventEmitter, OnInit, Output, Input, Inject, ChangeDetectionStrategy } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { ErrorsExtractor, translate } from '@rucken/core';
import { CodeVerify, CODE_VERIFIES_CONFIG_TOKEN } from '@mono-auth/core';
import { BsModalService } from 'ngx-bootstrap/modal';
import { DynamicRepository, IRestProviderOptions } from 'ngx-repository';
import { MessageModalService, IBaseEntityModalOptions } from '@rucken/web';
import { CodeVerifiesGridModalComponent } from '../code-verifies-grid-modal/code-verifies-grid-modal.component';
import { CodeVerifiesGridComponent } from '../code-verifies-grid/code-verifies-grid.component';


@Component({
  selector: 'code-verify-input',
  templateUrl: './code-verify-input.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class CodeVerifyInputComponent extends CodeVerifiesGridComponent implements OnInit {

  @Output()
  select = new EventEmitter <CodeVerify> ();
  @Input()
  modalAppendFromGrid: IBaseEntityModalOptions = {
    component: CodeVerifiesGridModalComponent,
    initialState: {
      title: translate('Select code verify'),
      yesTitle: translate('Select')
    }
  };

  constructor(
    public modalService: BsModalService,
    protected errorsExtractor: ErrorsExtractor,
    protected translateService: TranslateService,
    protected dynamicRepository: DynamicRepository,
    protected messageModalService: MessageModalService,
    @Inject(CODE_VERIFIES_CONFIG_TOKEN) protected codeVerifiesConfig: IRestProviderOptions<CodeVerify>
  ) {
    super(
      modalService,
      errorsExtractor,
      translateService,
      dynamicRepository,
      messageModalService,
      codeVerifiesConfig
    );
  }
  ngOnInit() {
    this.mockedItems = [];
    this.useMock({
      items: this.mockedItems,
      ...this.codeVerifiesConfig
    });
    this.mockedItemsChange.subscribe(items =>
      this.onSelect(items[0])
    );
  }
  onSelect(item: CodeVerify) {
    this.select.emit(item);
  }
}
