import { Provider } from '@angular/core';
import { CODE_VERIFIES_CONFIG_TOKEN, defaultCodeVerifiesConfig } from './code-verifies.config';
import { MOBILES_CONFIG_TOKEN, defaultMobilesConfig } from './mobiles.config';
import { TESTINGS_CONFIG_TOKEN, defaultTestingsConfig } from './testings.config';


export const entitiesProviders: Provider[] = [
  {
    provide: CODE_VERIFIES_CONFIG_TOKEN,
    useValue: defaultCodeVerifiesConfig
  },{
    provide: MOBILES_CONFIG_TOKEN,
    useValue: defaultMobilesConfig
  },{
    provide: TESTINGS_CONFIG_TOKEN,
    useValue: defaultTestingsConfig
  },
];
