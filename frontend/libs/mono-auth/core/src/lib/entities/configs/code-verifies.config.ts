import { Injectable } from '@angular/core';
import { plainToClass } from 'class-transformer';
import { IRestProviderOptions, PaginationMeta, ProviderActionEnum } from 'ngx-repository';
import { CodeVerify } from '../models/code-verify';

export const defaultCodeVerifiesConfig: IRestProviderOptions <CodeVerify> = {
  name: 'code_verify',
  pluralName: 'code_verifies',
  autoload: true,
  paginationMeta: {
    perPage: 5
  },
  actionOptions: {
    responseData: (data: any, action: ProviderActionEnum) => {
      if (action === ProviderActionEnum.Delete) {
        return true;
      } else {
        if (action === ProviderActionEnum.LoadAll) {
          return plainToClass(CodeVerify, data.body.codeVerifies);
        } else {
          return plainToClass(CodeVerify, data.body.codeVerify);
        }
      }
    },
    responsePaginationMeta: (data: any, action: ProviderActionEnum): PaginationMeta => {
      return { totalResults: data.body.meta.totalResults, perPage: undefined };
    }
  },
  restOptions: {
    limitQueryParam: 'per_page',
    pageQueryParam: 'cur_page',
    searchTextQueryParam: 'q'
  }
};
export const CODE_VERIFIES_CONFIG_TOKEN = 'CodeVerifiesConfig';
