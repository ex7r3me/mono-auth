import { IsNotEmpty } from 'class-validator';
import { IModel } from 'ngx-repository';
import { translate } from '@rucken/core';

export class CodeVerify implements IModel {
  static strings = {
    id: translate('Id'),
    name: translate('Name'),

    createTitle: translate('Add new code verify'),
    viewTitle: translate('Code verify #{{id}}'),
    updateTitle: translate('Update code verify #{{id}}'),
    deleteTitle: translate('Delete code verify #{{id}}'),
    deleteMessage: translate('Do you really want to delete code verify?')
  };
  id: number = undefined;
  @IsNotEmpty()
  name: string = undefined;

  toString() {
    return this.name;
  }
}
