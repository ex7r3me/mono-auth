import { IsNotEmpty } from 'class-validator';
import { IModel } from 'ngx-repository';
import { translate } from '@rucken/core';

export class Mobile implements IModel {
  static strings = {
    id: translate('Id'),
    name: translate('Name'),

    createTitle: translate('Add new mobile'),
    viewTitle: translate('Mobile #{{id}}'),
    updateTitle: translate('Update mobile #{{id}}'),
    deleteTitle: translate('Delete mobile #{{id}}'),
    deleteMessage: translate('Do you really want to delete mobile?')
  };
  id: number = undefined;
  @IsNotEmpty()
  name: string = undefined;

  toString() {
    return this.name;
  }
}
