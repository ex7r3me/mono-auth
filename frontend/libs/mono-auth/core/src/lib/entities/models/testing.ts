import { IsNotEmpty } from 'class-validator';
import { IModel } from 'ngx-repository';
import { translate } from '@rucken/core';

export class Testing implements IModel {
  static strings = {
    id: translate('Id'),
    name: translate('Name'),

    createTitle: translate('Add new testing'),
    viewTitle: translate('Testing #{{id}}'),
    updateTitle: translate('Update testing #{{id}}'),
    deleteTitle: translate('Delete testing #{{id}}'),
    deleteMessage: translate('Do you really want to delete testing?')
  };
  id: number = undefined;
  @IsNotEmpty()
  name: string = undefined;

  toString() {
    return this.name;
  }
}
